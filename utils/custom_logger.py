import sys
import logging

class CustomLogger:

    def __init__(self):
        logging.getLogger().setLevel(logging.INFO)

    # Taken from https://stackoverflow.com/questions/28330317/print-timestamp-for-logging-in-python
    @staticmethod
    def setup_customer_logger(name):
        """ Config for logging module """
        formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s',
                                      datefmt='%Y-%m-%d %I:%M:%S %p')
        handler = logging.FileHandler('log.txt', mode='w')
        handler.setFormatter(formatter)
        screen_handler = logging.StreamHandler(stream=sys.stdout)
        screen_handler.setFormatter(formatter)
        logger = logging.getLogger(name)
        logger.addHandler(handler)
        logger.addHandler(screen_handler)
        return logger
