
import pytest
from src.twitter_scraper import TwitterScraper


@pytest.fixture(scope="module")
def tweets():
    ts = TwitterScraper(username='BillGates', tweet_count=20)
    # Override user's page (in case
    with open('resource/bill_gates.htm', 'r') as f:
        ts.users_page = f.read()

    return ts.get_tweets()


def test_tweet_return(tweets):
    """ Basic test that list of tweets returned"""
    assert tweets


def test_tweets(tweets):
    """ Test contents on Tweets"""
    for tweet in tweets:
        assert isinstance(tweet.hashtags, list)
        if tweet.hashtags:  # any() returns false if empty
            assert any('#' in s for s in tweet.hashtags)

        assert isinstance(tweet.link, str)
        assert isinstance(tweet.mentions, list)
        assert isinstance(tweet.tweet_text, str)

        assert isinstance(tweet.like_count, int)
        assert isinstance(tweet.reply_count, int)
        assert isinstance(tweet.retweet_count, int)

        assert tweet.like_count >= 0
        assert tweet.reply_count >= 0
        assert tweet.retweet_count >= 0
