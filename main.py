
import time
from utils.custom_logger import CustomLogger
from src.twitter_scraper import TwitterScraper


class Main:
    def __init__(self, username, num_of_tweets):
        self.log = CustomLogger().setup_customer_logger("TwitterScraper")
        self.ts = TwitterScraper(username=username, tweet_count=num_of_tweets)

    def run_app(self):
        # get_tweets will get a list of tweets of Tweet object
        # Override __str__ in order to print data in a more clean format
        [print(tweet) for tweet in self.ts.get_tweets()]

    def run_app_one(self):
        # get_tweets will get a list of tweets of Tweet object
        # Override __iter__ in order use dict() function
        start_time = time.time()
        [print(dict(tweet)) for tweet in self.ts.get_tweets()]
        end_time = time.time()
        print("Elapsed Time: {0:.3f}".format(end_time - start_time))

    def run_app_two(self):

        for tweet in self.ts.get_tweets():
            print("Tweet Date: {}".format(tweet.t_date))
            print("Tweet Time: {}".format(tweet.t_time))
            print("Tweeter: {}".format(tweet.tweeter))
            print("Tweet Text: {}".format(tweet.tweet_text))
            # etc...


if __name__ == '__main__':
    Main(username='BillGates', num_of_tweets=10).run_app()
