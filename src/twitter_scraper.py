
import sys
import time
import requests
import json
import logging
from bs4 import BeautifulSoup
from src.tweet import Tweet


class TwitterScraper:
    def __init__(self, username, tweet_count):
        self.log = logging.getLogger('TwitterScraper')
        self._user = username
        self._users_page = None
        self.session = None
        self.min_position = ''
        self.has_more_items = True
        self._tweet_count = ''
        self.tweet_count = tweet_count
        self.list_of_tweets = []

        # Probably doesnt matter
        self.new_latent_count = 0,
        self.focused_refresh_interval = 0

    @property
    def user(self):
        if not self._user:
            raise Exception('User has not been defined')
        else:
            return self._user

    @property
    def users_page(self):
        if not self._users_page:
            self._users_page = self.get_users_page()
            return self._users_page
        else:
            return self._users_page

    @users_page.setter
    def users_page(self, value):
        self._users_page = value

    @property
    def tweet_count(self):
        if not self._tweet_count:
            raise Exception("Tweet count is [{}] and needs to be defined".format(self._tweet_count))
        else:
            return int(self._tweet_count)

    @tweet_count.setter
    def tweet_count(self, tweet_count):
        # Handling tweet counts. Override tweet_count if value entered is greater than total for the user
        max_tweet_count = self.user_max_tweets_count()
        if tweet_count <= 0:
            raise ValueError("The desired tweet count must be greater or equal to 1")
        elif tweet_count < max_tweet_count:
            self._tweet_count = tweet_count
        elif tweet_count > max_tweet_count:
            self.log.info("Invalid tweet count for {}. Returning {} instead".format(self.user, max_tweet_count))
            self._tweet_count = max_tweet_count

    def get_users_page(self):
        """ Get request of user's page

            :returns: response object
        """
        url = "https://twitter.com/{}".format(self._user)
        headers = {
            'cache-control': "no-cache",
            'user-agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) "
                          "AppleWebkit/537 (KHTML, like Gecko) "
                          "Chrome/68.0.3440.75 Safari/537.36"
        }
        self.session = requests.Session()
        response = self.session.get(url=url, headers=headers)

        if response.status_code != 200:
            raise Exception("Failed to retrieve {}'s page"
                            "\nPage returned status code of {}".format(self._user, response.status_code))
        else:
            return response.text

    def _initial_request(self):
        """ Scrape initial batch of tweets.

            :returns: Dictionary of tweets
        """
        self.log.info("Beginning Twitter Scraper...")\

        # Scrape all of initial tweets
        soup = BeautifulSoup(self.users_page, features="html.parser")
        tweets_container = soup.find('ol', {'id': 'stream-items-id'})
        tweets = tweets_container.find_all('li', {'data-item-type': 'tweet'}, limit=self.tweet_count)

        # Translate each tweet into a Tweet object and parse
        self.list_of_tweets = [Tweet(tweet) for tweet in tweets]

        # and get the last max_position on the list
        self.min_position = tweets[-1].attrs['data-item-id']

        return self.list_of_tweets

    def get_tweets(self):
        self._initial_request()
        url = 'https://twitter.com/i/profiles/show/{}/timeline/tweets'.format(self._user)

        # Continue with scraping tweets if count > 20
        # Note: Since Twitter uses a Top-Down design (https://en.wikipedia.org/wiki/Top-down_and_bottom-up_design),
        # we are unable to pull all of a user's tweets - only 20 at a time.
        while self.has_more_items and len(self.list_of_tweets) < self.tweet_count:
            params = {'include_available_features': '1',
                      'include_entities': '1',
                      'max_position': self.min_position,  # The starting point for the next batch of 20 tweets
                      'reset_error_state': 'true'}

            try:
                response = self.session.get(url=url, params=params).text
                self._error_check(response=response)
                response_dict = json.loads(response)
            except Exception as e:
                print(repr(e))
                sys.exit(1)

            # Stage data required for next request
            self.min_position = response_dict['min_position']
            self.has_more_items = response_dict['has_more_items']
            next_batch_html = response_dict['items_html']

            # Parse new batch of tweets
            soup = BeautifulSoup(next_batch_html, features="html.parser")
            limit = self.tweet_count - len(self.list_of_tweets)
            batch_of_tweets = \
                [Tweet(tweet) for tweet in soup.find_all('li', {'data-item-type': 'tweet'}, limit=limit)]
            self.list_of_tweets = self.list_of_tweets + batch_of_tweets
            self.log.info(len(self.list_of_tweets))

        self.log.info('{} Tweets Scraped'.format(len(self.list_of_tweets)))
        # total_size = 0
        # for n, tweet in enumerate(self.list_of_tweets, 1):
        #     # self.log.info('\n===========Size of Object============\n\tType: {}\tSize: {} bytes'
        #     #               .format(type(tweet), sys.getsizeof(tweet)))
        #     # total_size += int(sys.getsizeof(tweet))
        #     self.log.info("\n=============== TWEET #{} ==============="
        #                   "\nTweet Text: {}\nMentions: {}\nHashtags: {}\nLinks: {}"
        #                   .format(n, tweet.tweet_text, tweet.mentions, tweet.hashtags, tweet.link))

        # self.log.info('\n==========================================\n'
        #               '\t\tTotal: {} Bytes'.format(total_size))
        return self.list_of_tweets

    def user_max_tweets_count(self):
        """ Get max number of tweets for given user

            :returns: (int) Max number of tweets
        """
        # response used in order to reduce http calls

        soup = BeautifulSoup(self.users_page, features="html.parser")
        return int(soup.find('span', {'class': 'ProfileNav-value'}).attrs['data-count'])

    @staticmethod
    def _error_check(response):
        if "Uh-oh! You're being rate" in response:
            time.sleep(1)
