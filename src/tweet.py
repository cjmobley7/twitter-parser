
import re
import logging
import datetime


class TweetDateTime:
    def __init__(self, date_text):
        # Receive date_text from twitter in HH:MM TM - D Mon YYY (ex. 2:59 AM - 9 Dec 2018)
        # ...and parse into datetime object
        self._datetime = datetime.datetime.strptime(date_text, "%I:%M %p - %d %b %Y")

    @property
    def datetime(self):
        if not self._datetime:
            raise Exception("Failed to set Datetime")
        else:
            return self._datetime

    @property
    def tweet_date(self):
        """ Get date and return in more friendly format"""
        if self._datetime:
            return self._datetime.strftime('%m/%d/%Y')
        else:
            raise Exception("Failed to retrieve date from datetime")

    @property
    def tweet_time(self):
        """ Get time and return str format """
        if self._datetime:
            return self._datetime.strftime('%I:%M %p')


class Tweet:
    def __init__(self, tweet):
        self.log = logging.getLogger('TwitterScraper')
        self._tweet = tweet
        self._tweet_main = tweet.find('div', {'class': 'js-tweet-text-container'})
        self._tweet_header = tweet.find('div', {'class': 'stream-item-header'})
        self._tweet_footer = tweet.find('div', {'class': 'stream-item-footer'})
        self.tweet_text = self._tweet_main.text.strip('\n')
        self._mentions = []
        self._hashtags = []
        self._link = ''
        self._date = ''
        self._time = ''

        # Handle retweets
        self.is_a_retweet = False
        self.tweeter = ''

        # Tweet Footer Count
        self.reply_count = None
        self.retweet_count = None
        self.like_count = None

        # Scrape and populate data
        self.parse_tweet()

    # Override methods
    def __str__(self):
        return "\n\nDate: {} - Time: {}\nTweeter: {}\nTweet: {}\nHashtag(s): {} - Mention(s): {}\nLinks: {}" \
               "\nReply Count: {} - Retweet Count: {} - Like Count: {}\nIs a Retweet: {}"\
            .format(self._date, self._time, self.tweeter, self.tweet_text, self.hashtags, self.mentions, self.link,
                    self.reply_count, self.retweet_count, self.like_count, self.is_a_retweet)

    def __iter__(self):
        # Override generator in dict(). Allows for user to use dict on Tweet object in order to retrieve a dictionary
        # of all public attrs and their values
        attr_ignore = ['log', 'parse_tweet']
        for name in dir(self):
            if not name.startswith('_') and name not in attr_ignore:
                yield (name, getattr(self, name))

    # Properties/Getters
    @property
    def mentions(self):
        return self._mentions

    @property
    def hashtags(self):
        return self._hashtags

    @property
    def link(self):
        return self._link

    @property
    def t_time(self):
        return self._time

    @property
    def t_date(self):
        return self._date

    # Private setters for parsing tweet (TODO some of these variable may need to be private)
    def _set_mentions(self):
        try:
            self._mentions = re.findall(r'@\w+', self._tweet_main.text)
        except Exception as e:
            self.log.debug("No Mentions found for tweet: {}\nError: {}".format(self._tweet_main.text, e))

    def _set_hashtags(self):
        try:
            self._hashtags = re.findall(r'#\w+', self._tweet_main.text)
        except Exception as e:
            self.log.debug("No Hashtags found for tweet: {}\nError: {}".format(self._tweet_main.text, e))

    def _set_links(self):
        try:
            self._link = self._tweet_main.find('span', {'class': 'js-display-url'}).text
        except Exception as e:
            self.log.debug("No Links found for tweet: {}\nError: {}".format(self._tweet_main.text, e))

    def _set_reply_count(self):
        try:
            self.reply_count = int(re.search(r'(?P<reply_count>\d{1,3}(,\d{3})*(\.\d+)?) repl(ies|y)',
                                             self._tweet_footer.text).group('reply_count').replace(',', ''))
        except Exception as e:
            self.log.debug("No Reply Count found for tweet: {}\nError: {}".format(self._tweet_footer.text, e))

    def _set_retweet_count(self):
        try:
            self.retweet_count = int(re.search(r'(?P<retweet_count>\d{1,3}(,\d{3})*(\.\d+)?) retweets',
                                               self._tweet_footer.text).group('retweet_count').replace(',', ''))
        except Exception as e:
            self.log.debug("No Reply Count found for tweet: {}\nError: {}".format(self._tweet_footer.text, e))

    def _set_like_count(self):
        try:
            self.like_count = int(re.search(r'(?P<like_count>\d{1,3}(,\d{3})*(\.\d+)?) like[s]?',
                                            self._tweet_footer.text).group('like_count').replace(',', ''))
        except Exception as e:
            self.log.debug("No Reply Count found for tweet: {}\nError: {}".format(self._tweet_footer.text, e))

    def _set_date(self):
        try:
            date_raw = self._tweet_header.select('a[class*="tweet-timestamp"]')[0].attrs.get('title')
            tdt = TweetDateTime(date_raw)

            self._date = tdt.tweet_date
            self._time = tdt.tweet_time

        except Exception as e:
            self.log.debug("Something went wrong in getting or setting time...")
            self.log.debug(repr(e))

    def _check_if_retweet(self):
        try:
            self.is_a_retweet = True if self._tweet.find('span', {'class': 'js-retweet-text'}) else False
        except Exception as e:
            self.log.debug("Not a retweet... Returning False".format(e))
            return False

    def _set_tweeter(self):
        try:
            self.tweeter = re.search(r'(?P<tweeter>@\w+)', self._tweet_header.text).group(0)
        except Exception as e:
            self.log.debug("Unable to retrieve tweeter")
            self.log.debug(repr(e))

    # Parser
    def parse_tweet(self):
        """ Parse Tweet

            Compile the following attributes:
                - (str) Tweet text
                - (list) Link(s) within tweet
                - (list) Hashtag(s)
                - (list) Mentions
                - (int) Like Count
                - (str) Link
                - (int) Reply Count
                - (int) Retweet Count

            :returns: Tweet object containing attributes listed above
         """

        # Attributes within header
        self._set_date()
        self._set_tweeter()

        # Attributes within tweet text
        self._set_links()
        self._set_hashtags()
        self._set_mentions()

        # Footer Values
        self._set_reply_count()
        self._set_retweet_count()
        self._set_like_count()
        self._check_if_retweet()

        return self
