# twitter-parser-2018
A Python script that scrapes user's tweets

### Requirements/Install
- Python3
- Install requirements
    - `pip install -r requirements.txt`

### Run app
1. Set the username and number of desired tweets in main.py

    `Main(username='BillGates', num_of_tweets=1).run_app()`

2. Run the app

    `python3 main.py`

3. Sample Output
    ```
    2019-01-09 07:34:14 PM INFO     Beginning Twitter Scraper...
    2019-01-09 07:34:14 PM INFO     1 Tweets Scraped


    Date: 01/09/2019 - Time: 09:13 AM
    Tweeter: @BillGates
    Tweet: There's no question that it was a tough year for a lot of people. But @NickKristof makes a convincing case for why 2018 was actually the best year in human history.https://b-gat.es/2RI0DRy 
    Hashtag(s): [] - Mention(s): ['@NickKristof']
    Links: b-gat.es/2RI0DRy
    Reply Count: 123 - Retweet Count: 346 - Like Count: 1615
    Is a Retweet: False
    ```
    
### Twitter Parser usage
1. Import and instantiate TwitterParser

    `ts = TwitterScraper(username='BillGates', tweet_count=1)`
    
2. Get list of tweets

    `tweets = ts.get_tweets()`
    
3. Iterate through list of tweets and retrieve scraped data. Each tweet in list of tweets is a Tweet object.
    However, there are 2 main ways to get raw data...
    
    a. Using python's dict()
        
        >>> [print(dict(tweet)) for tweet in ts.get_tweets()]
        
        {'hashtags': [], 'is_a_retweet': False, 'like_count': 1667, 'link': 'b-gat.es/2RI0DRy', 'mentions': ['@NickKristof'], 'reply_count': 125, 'retweet_count': 359, 't_date': '01/09/2019', 't_time': '09:13 AM', 'tweet_text': "There's no question that it was a tough year for a lot of people. But @NickKristof makes a convincing case for why 2018 was actually the best year in human history.https://b-gat.es/2RI0DRy\xa0", 'tweeter': '@BillGates'}

    b. Using Tweet object's attributes
        
        >>> [print(tweet.tweet_text) for tweet in self.ts.get_tweets()]
        
        There's no question that it was a tough year for a lot of people. But @NickKristof makes a convincing case for why 2018 was actually the best year in human history.https://b-gat.es/2RI0DRy 

    Available attributes (as seen in dict):
    
        - t_date
        - t_time
        - hashtags
        - is_a_retweet
        - tweeter
        - like_count
        - link
        - mentions
        - reply_count
        - retweet_count
        - tweet_text

### Limitations

Runtime. Since the max number of tweets that can retrieve per request is 20, then we are constrained by the time 
it takes to send and receive data. Runtime depends largely on connection and how often twitter
will Rate Limit. On average, time-frame are as follows: ~1.9 sec/100 Tweets, ~5 sec/250 tweets, ~9 sec/500 tweets, 
~18 sec/ 1000 Tweets. One possible way to improve this time is to get the min/max position of each batch of 
tweets. This min/max position dictates the head and tail of the stack for each batch. This will greatly reduce runtime 
as we can run each request at parallel...might still get rate limited through



